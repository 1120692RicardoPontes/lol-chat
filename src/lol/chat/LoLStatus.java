/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lol.chat;

/**
 *
 * @author Ricardo
 */
public class LoLStatus {

    private int icon = 0;
    private String status = "";
    private int level = 0;
    private int wins = 0;
    private int leaves = 0;
    private String queueType = "";
    private int rankedWins = 0;
    private int rankedLosses = 0;
    private int rankedRating = 0;
    private String rankedLeagueName = "";
    private String rankedLeagueDivision = "";
    private String tier = "";
    private String gameStatus = "";

    public LoLStatus(int icon, String status, int level, int wins, int leaves, String queueType, int rankedWins, int rankedLosses, int rankedRating, String rankedLeagueName, String rankedLeagueDivision, String tier, String gameStatus) {
        this.icon = icon;
        this.status = status;
        this.level = level;
        this.wins = wins;
        this.leaves = leaves;
        this.queueType = queueType;
        this.rankedWins = rankedWins;
        this.rankedLosses = rankedLosses;
        this.rankedRating = rankedRating;
        this.rankedLeagueName = rankedLeagueName;
        this.rankedLeagueDivision = rankedLeagueDivision;
        this.tier = tier;
        this.gameStatus = gameStatus;
    }

    public LoLStatus(String code) {
        if (code.isEmpty()) {
            return;
        }
        try {
            gameStatus = getValueAt("gameStatus", code);
            status = getValueAt("statusMsg", code);
            icon = Integer.parseInt(getValueAt("profileIcon", code));
            level = Integer.parseInt(getValueAt("level", code));
            wins = Integer.parseInt(getValueAt("wins", code));
            leaves = Integer.parseInt(getValueAt("leaves", code));
            queueType = getValueAt("rankedLeagueQueue", code);
            rankedWins = Integer.parseInt(getValueAt("rankedWins", code));
            rankedLosses = Integer.parseInt(getValueAt("rankedLosses", code));
            rankedRating = Integer.parseInt(getValueAt("rankedRating", code));
            tier = getValueAt("rankedLeagueTier", code);
            rankedLeagueName = getValueAt("rankedLeagueName", code);
            rankedLeagueDivision = getValueAt("rankedLeagueDivision", code);
        }
        catch (Exception ex) {
        }
    }

    private String getValueAt(String tag, String where) {
        try {
            int i = where.indexOf(tag) + tag.length() + 1;
            int ii = where.indexOf(tag, i + 1) - 2;
            return where.substring(i, ii);
        }
        catch (Exception ex) {
            //System.out.println("getValueAt:\n\ttag: " + tag + "\n\twhere: " + where + "\n\t" + ex.getMessage() + "\n");
        }
        return "";
    }

    @Override
    public String toString() {
        /* <body>
         * <profileIcon>503</profileIcon>
         * <level>30</level>
         * <wins>604</wins>
         * <leaves>18</leaves>
         * <odinWins>1</odinWins>
         * <odinLeaves>1</odinLeaves>
         * <queueType />
         * <rankedLosses>0</rankedLosses>
         * <rankedRating>0</rankedRating>
         * <tier>UNRANKED</tier>
         * <statusMsg></statusMsg>
         * <skinname>Random</skinname>
         * <gameQueueType>NORMAL</gameQueueType>
         * <gameStatus>outOfGame</gameStatus>
         * <rankedLeagueName>JaxsParagons</rankedLeagueName>
         * <rankedLeagueDivision>IV</rankedLeagueDivision>
         * <rankedLeagueTier>SILVER</rankedLeagueTier>
         * <rankedLeagueQueue>RANKED_SOLO_5x5</rankedLeagueQueue>
         * <rankedWins>51</rankedWins>
         * </body>
         */
        String msg = "<body>";
        msg += "<profileIcon>" + icon + "</profileIcon>";
        msg += "<statusMsg>" + status + "</statusMsg>";
        msg += "<level>" + level + "</level>";
        msg += "<wins>" + wins + "</wins>";
        msg += "<leaves>" + leaves + "</leaves>";
        msg += "<queueType />";
        msg += "<rankedWins>" + rankedWins + "</rankedWins>";
        msg += "<rankedLosses>" + rankedLosses + "</rankedLosses>";
        msg += "<rankedRating>" + rankedRating + "</rankedRating>";
        msg += "<tier>" + tier + "</tier>";
        msg += "<rankedLeagueName>" + rankedLeagueName + "</rankedLeagueName>";
        msg += "<rankedLeagueDivision>" + rankedLeagueDivision + "</rankedLeagueDivision>";
        msg += "<rankedLeagueTier>" + tier + "</rankedLeagueTier>";
        msg += "<rankedLeagueQueue>" + queueType + "</rankedLeagueQueue>";
        msg += "<gameStatus>" + gameStatus + "</gameStatus>";
        msg += "</body>";
        return msg;
    }

    public int getIcon() {
        return icon;
    }

    public int getLeaves() {
        return leaves;
    }

    public int getLevel() {
        return level;
    }

    public String getQueueType() {
        return queueType;
    }

    public int getRankedLosses() {
        return rankedLosses;
    }

    public int getRankedRating() {
        return rankedRating;
    }

    public int getRankedWins() {
        return rankedWins;
    }

    public String getStatus() {
        return status;
    }

    public String getTier() {
        return tier;
    }

    public int getWins() {
        return wins;
    }

    public String getGameStatus() {
        return gameStatus;
    }

    public String getRankedLeagueDivision() {
        return rankedLeagueDivision;
    }

    public String getRankedLeagueName() {
        return rankedLeagueName;
    }

    public String getLeagueDivision() {
        return rankedLeagueDivision;
    }

    public String getLeagueName() {
        return rankedLeagueName;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public void setLeaves(int leaves) {
        this.leaves = leaves;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public void setQueueType(String queueType) {
        this.queueType = queueType;
    }

    public void setRankedLosses(int rankedLosses) {
        this.rankedLosses = rankedLosses;
    }

    public void setRankedRating(int rankedRating) {
        this.rankedRating = rankedRating;
    }

    public void setRankedWins(int rankedWins) {
        this.rankedWins = rankedWins;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setTier(String tier) {
        this.tier = tier;
    }

    public void setWins(int wins) {
        this.wins = wins;
    }

    public void setLeagueDivision(String leagueDivision) {
        this.rankedLeagueDivision = leagueDivision;
    }

    public void setLeagueName(String leagueName) {
        this.rankedLeagueName = leagueName;
    }

    public void setGameStatus(String gameStatus) {
        this.gameStatus = gameStatus;
    }

    public void setRankedLeagueDivision(String rankedLeagueDivision) {
        this.rankedLeagueDivision = rankedLeagueDivision;
    }

    public void setRankedLeagueName(String rankedLeagueName) {
        this.rankedLeagueName = rankedLeagueName;
    }
}
