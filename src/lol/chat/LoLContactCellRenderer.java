/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lol.chat;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;

/**
 *
 * @author Ricardo
 */
public class LoLContactCellRenderer implements ListCellRenderer {

    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        if (value instanceof LoLContact) {
            LoLContact contact = (LoLContact) value;
            JPanel panel = new JPanel(new BorderLayout());
            if (isSelected) {
                panel.setBackground(Color.LIGHT_GRAY);
            }
            else {
                panel.setBackground(Color.WHITE);
            }
            panel.add(new JLabel(contact.getName()), BorderLayout.WEST);
            String status = contact.getFormattedGameStatus();
            JLabel lblStatus = new JLabel(status);
            switch (status) {
                case "inGame":
                    lblStatus.setForeground(Color.BLUE);
                    break;
                case "Away":
                    lblStatus.setForeground(Color.RED);
                    break;
                default:
                    lblStatus.setForeground(Color.BLACK);
            }
            panel.add(lblStatus, BorderLayout.EAST);
            return panel;
        }
        else {
            return new JLabel(value.toString());
        }
    }
}
