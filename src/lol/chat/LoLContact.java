/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lol.chat;

import java.util.Objects;
import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.packet.Presence;

/**
 *
 * @author Ricardo
 */
public class LoLContact {

    private RosterEntry entry;
    private String user;
    private String name;
    private Presence presence;

    public LoLContact(RosterEntry entry, Presence presence) {
        this.entry = entry;
        this.presence = presence;
        user = entry.getUser();
        name = entry.getName();
    }

    public LoLStatus getStatus() {
        return new LoLStatus(presence.getStatus());
    }

    public String getFormattedGameStatus() {
        LoLStatus s = getStatus();
        Presence.Mode m = presence.getMode();
        if (m != Presence.Mode.away) {
            if (s.getGameStatus().equals("outOfGame")) {
                return s.getStatus();
            }
            else {
                return s.getGameStatus();
            }
        }
        else {
            return "Away";
        }
    }

    public RosterEntry getEntry() {
        return entry;
    }

    public String getName() {
        return name;
    }

    public Presence getPresence() {
        return presence;
    }

    public String getUser() {
        return user;
    }

    public boolean isOnline() {
        return presence.isAvailable();
    }

    public String getStatusMessage() {
        return presence.getStatus();
    }

    @Override
    public String toString() {
        return name + ":\n\tUser: " + user + "\n\tOnline: " + isOnline() + "\n\tStatus: " + getStatusMessage();
    }

    @Override
    public boolean equals(Object obj) {
        return ((LoLContact) obj).getUser().equals(user);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + Objects.hashCode(this.user);
        return hash;
    }
}
