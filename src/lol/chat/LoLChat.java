/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lol.chat;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.UIManager;
import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.ChatManagerListener;

/**
 *
 * @author Ricardo
 */
public class LoLChat {

    private static String info = "<body><profileIcon>503</profileIcon><level>30</level><wins>1337</wins><leaves>0</leaves><odinWins>0</odinWins><odinLeaves>0</odinLeaves><queueType /><rankedLosses>0</rankedLosses><rankedRating>9000</rankedRating><tier>UNRANKED</tier><statusMsg></statusMsg><skinname>Random</skinname><gameQueueType>NORMAL</gameQueueType><gameStatus>outOfGame</gameStatus><rankedLeagueName>JaxsParagons</rankedLeagueName><rankedLeagueDivision>I</rankedLeagueDivision><rankedLeagueTier>CHALLENGER</rankedLeagueTier><rankedLeagueQueue>RANKED_SOLO_5x5</rankedLeagueQueue><rankedWins>1337</rankedWins></body>";
    public static JTextField txtStatus;
    public static LoLChatAPI api;
    private static ContactsWindow contactsWindow;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            // Set System L&F
            UIManager.setLookAndFeel(
                    UIManager.getSystemLookAndFeelClassName());
        }
        catch (Exception e) {
        }


        api = new LoLChatAPI();
        LoginDialog login = new LoginDialog(api, new JFrame(), true);
        login.setVisible(true);
        api.setStatus(info);



        contactsWindow = new ContactsWindow(api);
        contactsWindow.setVisible(true);
        contactsWindow.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {
            }

            @Override
            public void windowClosing(WindowEvent e) {
                api.disconnect();
            }

            @Override
            public void windowClosed(WindowEvent e) {
            }

            @Override
            public void windowIconified(WindowEvent e) {
            }

            @Override
            public void windowDeiconified(WindowEvent e) {
            }

            @Override
            public void windowActivated(WindowEvent e) {
            }

            @Override
            public void windowDeactivated(WindowEvent e) {
            }
        });

        api.addChatListener(new ChatManagerListener() {
            @Override
            public void chatCreated(Chat chat, boolean createdLocally) {
                if (!api.hasChat(chat.getParticipant())) {
                    ChatWindow cw = new ChatWindow(api, chat);
                    cw.setVisible(true);
                }
            }
        });



    }
}
