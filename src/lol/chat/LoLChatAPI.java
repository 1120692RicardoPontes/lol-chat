/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lol.chat;

import java.util.*;

import org.jivesoftware.smack.ChatManager;
import org.jivesoftware.smack.ChatManagerListener;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.RosterListener;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Presence;

/**
 *
 * @author Ricardo
 */
public class LoLChatAPI {

    private XMPPConnection connection;
    private Presence presence;
    private ChatManager chat;
    private Roster roster;
    private static ArrayList<String> chats = new ArrayList<>();

    public void login(String userName, String password) throws XMPPException {
        ConnectionConfiguration config = new ConnectionConfiguration("chat.eu.lol.riotgames.com", 5223, "pvp.net");
        config.setSecurityMode(ConnectionConfiguration.SecurityMode.enabled);
        config.setSocketFactory(new DummySSLSocketFactory());
        connection = new XMPPConnection(config);
        connection.connect();
        connection.login(userName, "AIR_" + password);
        presence = new Presence(Presence.Type.available, "", 0, Presence.Mode.available);
        chat = connection.getChatManager();
        roster = connection.getRoster();
        roster.setSubscriptionMode(Roster.SubscriptionMode.accept_all);
    }

    public void addChatListener(ChatManagerListener l) {
        chat.addChatListener(l);
    }

    public void addRosterListener(RosterListener l) {
        roster.addRosterListener(l);
    }

    public void createChat(LoLContact c) {
        if (!chats.contains(c.getUser())) {
            chat.createChat(c.getUser(), null);
            chats.add(c.getUser());
        }
    }

    public boolean hasChat(String user) {
        return chats.contains(user);
    }

    public void deleteChat(String c) {
        chats.remove(c);
    }

    public String getUserName(String user) {
        return roster.getEntry(user).getName();
    }

    public ArrayList<LoLContact> getBuddyList() {
        ArrayList<LoLContact> contacts = new ArrayList<>();
        Collection<RosterEntry> entries = roster.getEntries();
        for (RosterEntry r : entries) {
            Presence pres = roster.getPresence(r.getUser());
            LoLContact contact = new LoLContact(r, pres);
            if (contact.isOnline()) {
                contacts.add(contact);
            }
        }
        return contacts;
    }

    public void disconnect() {
        connection.disconnect();
    }

    public void setStatus(String info) {
        presence.setStatus(info);
        connection.sendPacket(presence);
    }

    public void setStatus(LoLStatus status) {
        setStatus(status.toString());
    }

    public LoLStatus getStatus() {
        return new LoLStatus(presence.getStatus());
    }
}
